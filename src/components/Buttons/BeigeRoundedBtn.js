import * as React from 'react';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';

const BeigeRoundedBtn = () => {
  return (
    <Button variant="outlined" startIcon={<DeleteIcon />} style={{color:"#EABF9F"}}>
    Delete
    </Button>
  )
}

export default BeigeRoundedBtn;