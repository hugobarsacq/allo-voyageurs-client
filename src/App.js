import Home from './pages/Home/Home';
import NavBar from './components/NavBar/NavBar';
import HelloVoyageur from './components/HelloVoyageur/HelloVoyageur';
import Chat from './components/Chat/Chat';
import Auth from './pages/Auth/Auth';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/map" exact component={HelloVoyageur} />
          <Route path="/profil" exact component={Chat} />
          <Route path="/auth" exact component={Auth} />
          {/*           <Route path="/map" exact component={Map} />
          <Route path="/profil" exact component={Profil} />
          <Route path="/ad" exact component= {Ad} />
          <Route path="/spot" exact component={Spot} /> */}
        </Switch>
      </div>
      <NavBar />

    </BrowserRouter>
  );
}

export default App;
